import argparse
from typing import TextIO


def commonality(report):
    sums = []
    for number in report:
        for digit_index, digit in enumerate(number):
            if len(sums) <= digit_index:
                sums.append(0)
            sums[digit_index] += int(digit)
    most_common = ['1' if x >= len(report) / 2 else '0' for x in sums]
    least_common = ['0' if x == '1' else '1' for x in most_common]

    return most_common, least_common


def part_1(input_file: TextIO):
    input_text = input_file.read()
    report = input_text.split('\n')
    most_common, least_common = commonality(report)

    gamma = int(''.join(most_common), base=2)
    epsilon = int(''.join(least_common), base=2)

    return gamma * epsilon


def part_2(input_file: TextIO):
    input_text = input_file.read()
    report = input_text.split('\n')

    ogr_report = report
    digit_index = 0
    while len(ogr_report) > 1:
        most_common, _ = commonality(ogr_report)
        ogr_report = [x for x in ogr_report if x[digit_index] == most_common[digit_index]]
        digit_index += 1
    ogr = int(ogr_report[0], base=2)

    c02sr_report = report
    digit_index = 0
    while len(c02sr_report) > 1:
        _, least_common = commonality(c02sr_report)
        c02sr_report = [x for x in c02sr_report if x[digit_index] == least_common[digit_index]]
        digit_index += 1
    c02sr = int(c02sr_report[0], base=2)

    return ogr * c02sr


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 3')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
